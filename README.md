Sample PoC for A Cognito User Pool Setup.
-----------------------------------------

The callback URL must be setup in cognito for this app client. The app client must not have a secret configured.

Set the App Client ID and Cognito User Pool Id in the file `userprofile.js`

    var appClientId = "<my client id>";
    var userPoolId = "<region>_<pool_id>";
